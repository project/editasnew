This is a relatively simple module. It allows you to use an existing
node as a template when creating a new one.

It offers up two node hooks:

editasnew_create
  This gets called before a new node form is initially displayed, but after
  the fields from the template node have been copied to it.

editasnew_insert
  This gets called immediately after a node has been saved to the database
  so that other modules can perform whatever cleanup needs to be done on it.

Usage:
Copy this module into your modules directory.
Enable the module in the admin/modules page.
Configure which node types can use this feature in admin/settings/editasnew.
Click on the "edit as new" tab for each node that you want to copy.

TODO:
add a hook that lets other modules list out what fields to exclude from being
copied.  Currently, 'nid','created','changed','type' are all skipped.  I'm sure
problems will arise in time as data gets trampled.
Update: Now, complicated flexinode types are skipped.

This is a first stab at implementing this feature. If you have suggestions,
requests, complaints, etc, post them to the issues list for this module
or write me at mark@nullcraft.org.

-Mark Howell

